FROM python:3.8.2
LABEL maintainer="Yunwen, Chen <yunwenchenn@gmail.com>"

COPY ./app /app

WORKDIR /app

RUN pip install -r requirements.txt

RUN sed -i 's/CipherString = DEFAULT@SECLEVEL=2/CipherString = DEFAULT@SECLEVEL=1/g' /etc/ssl/openssl.cnf

CMD ["python", "app.py"]