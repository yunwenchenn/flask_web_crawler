flask: 591 rent web crawler

#調整爬蟲範圍：
file: crawler.py
line55: 可以設定crawler區間，因數據量較大的關係，
        目前設定為crawler所有物件數的個位數+10的數據，先做功能驗證使用。
        若要抓取所有數據內容，可把 #54的註解解開。

### Step 1: Download file
```
$ git clone https://gitlab.com/yunwenchenn/flask_web_crawler.git
```

### Step 2: to the folder 
```
$ cd flask_web_crawler/
```

### Step 3: Build Docker image
```
$ docker-compose build
```

### Step 4: Create docker network
```
docker network create crawler_web_external
```

### Step 5: Run service
```
$ docker-compose up -d
```

### Step6: db migrate
```
docker exec -it rent_591_crawler flask db init
docker exec -it rent_591_crawler flask db migrate
docker exec -it rent_591_crawler flask db upgrade
```

# how to use API
Method: POST
Content-type: x-www-form-urlencoded

- 1. 591 rent crawler
    http://localhost:5000/query_rent_detail
     - Key, Value = 'region_id', '01' or '03'
     
- 2. insert crawler data to DB
     http://localhost:5000/query_rent_detail
     - Key, Value = 'region_id',  '01'      #Taipei City
     - Key, Value = 'region_id',  '03'        #New Taipei City

### Step7: Stop service
```
$ docker-compose down
```