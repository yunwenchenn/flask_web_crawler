from database import db


class Rent_db(db.Model):
    __tablename__ = 'RENT'
    __table_args__ = (
        db.UniqueConstraint('title', 'owner_name', 'owner_type', 'building_type',
                            'room_type', name='unique_rent'),
    )
    id_ = db.Column("id", db.Integer, primary_key=True)
    title = db.Column("title", db.String(50))               # 承租標題
    address = db.Column("address", db.String(100))               # 承租地址
    rent_price = db.Column("rent_price", db.String(100))               # 承租金額
    owner_name = db.Column("owner_name", db.String(30))   # 承租人姓名
    owner_type = db.Column("owner_type", db.String(30))   # 承租人類型
    owner_cellphone = db.Column("owner_cellphone", db.String(30))  # 承租人電話
    building_type = db.Column("building_type", db.String(50))  # 建築物型態
    room_type = db.Column("room_type", db.String(50))  # 房間型態
    gender_require = db.Column("gender_require", db.String(30))  # 性別要求


