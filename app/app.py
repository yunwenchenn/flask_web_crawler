import os
from flask import Flask
from flask_migrate import Migrate
from crawler import rent_crawler
from database import db


def create_app():
    app = Flask(__name__)
    app.config.from_object("config.Config")
    db.init_app(app)
    migrate = Migrate()
    migrate.init_app(app, db)
    app.register_blueprint(rent_crawler)          # 爬蟲api

    # check health status
    @app.route('/')
    def health():
        return {'status': 'OK', 'message': 'Success', 'metadata': 'healthy'}
    return app


def setup_database(app):
    with app.app_context():
        db.create_all()


if __name__ == '__main__':
    app = create_app()
    port = int(os.environ.get('PORT', 5000))
    app.config['JSON_AS_ASCII'] = False
    app.run(host='0.0.0.0', port=port, debug=True)
