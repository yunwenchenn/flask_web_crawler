import requests
import collections
from bs4 import BeautifulSoup
from sqlalchemy.exc import IntegrityError
from flask import Blueprint, request, url_for
from fake_useragent import UserAgent
from config import Config as CONFIG
from models import Rent_db
from database import db

rent_crawler = Blueprint('rent_crawler', __name__,
                         template_folder='templates', static_folder='static')


def set_header_user_agent():
    user_agent = UserAgent()
    return user_agent.random


def get_total_row(region_id):
    user_agent = set_header_user_agent()
    url = ""
    if str(region_id) == '1':
        url = CONFIG.RENT_591_URL + str(region_id)
    elif str(region_id) == '3':
        url = CONFIG.RENT_591_URL + str(region_id)
    headers = {
        'User-Agent': user_agent
    }
    if not url:
        return
    res = requests.get(url, headers=headers)
    if res.status_code == 200:
        soup = BeautifulSoup(res.text, 'html.parser')
        total_row = soup.find(id='house_data_arr')['value']
    else:
        total_row = ""
    return {'total_row': total_row}

def get_house_id(url):
    user_agent = set_header_user_agent()
    headers = {
        'User-Agent': user_agent
    }
    res = requests.get(url, headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')
    soup.encoding = 'utf-8'
    house_data = soup.find_all('li', {"class": "data choose-li"})
    house_id = [(house_data[i]['data-house-id']).replace('R', '') for i in range(len(house_data))]
    return house_id


def get_house_id_all(region_id):
    #total_row = get_total_row(region_id)['total_row']
    total_row = int(get_total_row(region_id)['total_row'][-1:]) + 10
    print(total_row)
    house_ids = []
    for i in range(1, int(total_row), 8):
        region_url = CONFIG.RENT_591_URL + str(region_id) + '&firstRow=%s&totalRows=%s' % (i, total_row)
        house_id = get_house_id(region_url)
        house_ids = house_ids + house_id
    return house_ids


def get_house_url(region_id):
    house_ids = get_house_id_all(region_id)
    house_urls = []
    for i in house_ids:
        house_url = CONFIG.RENT_DETAIL_URL + '-%s.html' % i
        house_urls = house_urls + [house_url]
    print('geting house_urls: ', house_urls)
    return house_urls


@rent_crawler.route('/query_rent_detail', methods=['GET', 'POST'])
def query_rent_detail(**kwargs):
    if request.method == 'GET':
        return "Please use post method to get details.."
    if request.method == 'POST':
        result_dict = collections.OrderedDict()
        results = []
        user_agent = set_header_user_agent()
        headers = {
            'User-Agent': user_agent
        }
        region_id = request.form['region_id']
        house_urls = get_house_url(region_id)
        for url in house_urls:
            # get details
            res = requests.get(url, headers=headers)
            soup = BeautifulSoup(res.text, 'html.parser')
            soup.encoding = 'utf-8'
            title = soup.find('span', {"class": "houseInfoTitle"}).text
            address = soup.find('span', {"class": "addr"}).text.replace("<jk></jk>", "")
            rent_price = \
            soup.find('div', {'class', 'price clearfix'}).text.replace('\n', '').replace(' ', '').split('元')[0].replace(
                ',', '')
            ii = soup.find('ul', {'class', 'attr'})
            ii_col = []
            for index in range(
                    len([li.text.replace('\xa0', '').replace(' ', '').split(':') for li in ii.find_all('li')])):
                ii_col.append(([(li.text.replace('\xa0', '').replace(' ', '').split(':')[0] + ":" +
                                 li.text.replace('\xa0', '').replace(' ', '').split(':')[1])
                                for li in ii.find_all('li')][index]))
            owner = soup.find('div', {'class', 'avatarRight'}).find('div').text.replace('（', '(').replace('）', ')')
            owner_name = owner.split('(')[0]
            owner_type = owner.split('(')[1].replace(')', '')
            try:
                owner_cellphone = soup.find('span', {'class', 'dialPhoneNum'})['data-value'].replace('-', '')
            except:
                owner_cellphone = ""
            building_type = str([i.replace('型態:', '') for i in ii_col if '型態' in i]).replace('[', '').replace(']',
                                                                                                              '').replace(
                "'", '')
            room_type = str([i.replace('現況:', '') for i in ii_col if '現況' in i]).replace('[', '').replace(']',
                                                                                                          '').replace(
                "'", '')
            info_list = soup.find('ul', {'class', 'clearfix labelList labelList-1'})
            gender_require = [i.find('div', {'class', 'two'}).text.replace("：", "")
                              for i in info_list.find_all('li')
                              if i.find('div', {'class', 'one'}).text == "性別要求"]
            print('crawling.... : ', title)
            # result_dict
            result_dict['title'] = title
            result_dict['address'] = address
            result_dict['rent_price'] = rent_price
            result_dict['owner_name'] = owner_name
            result_dict['owner_type'] = owner_type
            result_dict['owner_cellphone'] = owner_cellphone
            result_dict['building_type'] = building_type
            result_dict['room_type'] = room_type
            result_dict['gender_require'] = gender_require
            dictionary_copy = result_dict.copy()
            results.append(dictionary_copy)
        return {'results': results}


# insert to db
@rent_crawler.route('/insert_db', methods=['GET', 'POST'])
def insert_db(**kwargs):
    region_id = request.form['region_id']
    user_agent = set_header_user_agent()
    headers = {
        'User-Agent': user_agent
    }
    payload = {
        'region_id': request.form.get('region_id'),
    }
    url = url_for('rent_crawler.query_rent_detail', _external=True)
    response = requests.request("POST", url=url, headers=headers, data=payload, verify=False)
    result = response.json()['results']
    insert_fail_list = []
    for i in range(len(result)):
        title = result[i]["title"]
        address = result[i]["address"]
        rent_price = result[i]["rent_price"]
        owner_name = result[i]["owner_name"]
        owner_type = result[i]["owner_type"]
        owner_cellphone = result[i]["owner_cellphone"]
        building_type = result[i]["building_type"]
        room_type = result[i]["room_type"]
        gender_require = result[i]["gender_require"]
        new_rent_details = Rent_db(title=title, address=address, rent_price=rent_price, owner_name=owner_name,
                                   owner_type=owner_type, owner_cellphone=owner_cellphone, building_type=building_type,
                                   room_type=room_type, gender_require=gender_require)
        try:
            db.session.add(new_rent_details)
            print('insert ok..', result[i]["title"])
        except:
            print('fail to insert data', insert_fail_list.append(result[i]["title"]))
    print(insert_fail_list)
    try:
        db.session.commit()
    except IntegrityError as e:
        print("IntegrityError", e)
    return response.json()
