import os
from dotenv import load_dotenv

load_dotenv()


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.getenv("POSTGRE_DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv(
        "SQLALCHEMY_TRACK_MODIFICATIONS")
    SECRET_KEY = os.getenv("SECRET_KEY")
    RENT_591_URL = os.getenv("RENT_591_URL")
    RENT_DETAIL_URL = os.getenv("RENT_DETAIL_URL")